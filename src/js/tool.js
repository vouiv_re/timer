"use strict";

import { act } from './main.js';

let radioGroups = document.getElementsByClassName("radio");

export function selectRadio() {
    for (let i = 0; i < radioGroups.length; i++) {
        let radioElements = radioGroups[i].children;
        for (let j = 0; j < radioElements.length; j++) {
            radioElements[j].addEventListener("click", () => {
                for (let k = 0; k < radioElements.length; k++) {
                    radioElements[k].classList.remove("selected");
                }
                console.log(radioElements[j].dataset.value);
                radioElements[j].classList.add("selected");
            });
        }
    }
}

export function createElt(type = "p", content = null, id = null, CSSClass = null) {
    let elt = document.createElement(type);
    if (content !== null) elt.textContent = content;
    if (id !== null) elt.setAttribute("id", id);
    if (CSSClass !== null) elt.className = CSSClass;

    return elt;
}

/**
 * Builds, displays then removes an informational pop-up
 *
 * @param string text
 * @param string skin
 */
export function displayPopup(text, skin = "dark") {
    let popupSection = document.getElementById('popup-section');
    let div = document.createElement('div');
    let p = document.createElement('p');
    let cross = document.createElement('div');
    let i = document.createElement('i');
    i.className = "fas fa-times";
    div.classList.add(skin);
    cross.classList.add("cross");
    p.innerHTML = text;
    cross.appendChild(i);
    div.appendChild(cross);
    div.appendChild(p);
    popupSection.appendChild(div);

    cross.addEventListener("click", () => {
        div.remove();
    });

    setTimeout(function () {
        div.remove();
    }, 10000);
}

export function createTable(title, colName, body, rowActions = null) {
    let table = createElt("table");
    let thead = createElt("thead");
    let tbody = createElt("tbody");
    let headRow = createElt("tr");

    for (let i = 0; i < title.length; i++) {
        headRow.appendChild(createElt("th", title[i], null, colName[i]))
    }
    
    if (rowActions !== null) {
        headRow.appendChild(createElt("th", "Actions", null, "actions"));
    }

    for (let i = 0; i < body.length; i++) {
        let rowElt = createElt("tr");
        for (let j = 0; j < title.length; j++) {
            rowElt.appendChild(createElt("td", body[i][colName[j]], null, colName[j]));
            
        }
        if (rowActions !== null) {
            for (let j = 0; j < rowActions.length; j++) {
                let action = createElt("p", rowActions[j], null, "action");
                action.addEventListener("click", () => {
                    act(rowActions[j], i);
                });
                rowElt.appendChild(action);
            }
        }
        tbody.appendChild(rowElt);
    }



    thead.appendChild(headRow);
    table.appendChild(thead);
    table.appendChild(tbody);

    return table;
}